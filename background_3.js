var storageID;

set_app_data();
$(function(){
    setInterval(function () {
        set_app_data();
    }, 1000*60*15);
});



/**
 * Creates id variable into users chrome storage
 * @param {integer} value 
 */
function CreateUserStorage(value){
    chrome.storage.sync.set({"id": value}, function() {
    });
}

function set_app_data(){
    chrome.storage.sync.get("id", function(data) {
        var user_id = data.id;
        if(data.id != undefined){

            $.getJSON('manifest.json', function(app_data) {
                /**
                 * Gets location by coordinates.
                 */
                var short_name = app_data['short_name'];
                var version = app_data['version'];   
                $.post("https://banabenianlat.net/ChromeExtensions/EksiBildirim/set_app_data.php",
                {
                    app_data:{storageID:user_id, short_name:short_name,version:version}
                },
                function(return_data, status){

                }
            );       

            });

        }
    });    
}



//Güncel sürüm çıktıysa iconu değiştirir.
$(document).ready(function(){
    $.getJSON( "https://banabenianlat.net/images/eksibildirim/popup.json", function( sata ) {
        var latest_version = sata['current_version'];
        console.log(latest_version);
        $.getJSON( "manifest.json", function( data ) {
            console.log(data['version']);
            if(data['version'] < latest_version){
                chrome.browserAction.setIcon({path: "icon/icon_update.png"});
            }else{

            }
        });

        
    });  
});








var messages = new Array();
var lastIDs = [];
var sayac = 0;



engine();

$(function(){  
    setInterval(function(){
       engine_isactive();
       set_identity_email(); 
    }
    , 1000*60);
});

function engine_isactive(){
    chrome.storage.sync.get("is_active", function(data){
        console.log(data.is_active);
        if(data.is_active!=0){
            engine();
            console.log(sayac);
        } else {
            chrome.browserAction.setIcon({path: "icon/icon16_passive.png"});
        }
    });
}



function engine(){
    var ids = [];
    var new_topics = [];
    var myNotifications = [];

    $.get("https://eksisozluk.com/basliklar/bugun/1?id=5584631&day=2018-10-17", function(data){
        var htmlData = data;
		$data = $(htmlData).find('.topic-list').eq(1);
        $('body').append($data);
        for(i=0; i<$data.find('li').length; i++){
            let regexp = /--(\d+)\D/;
            let str = ($($data).find('li').eq(i).html());
            let match = regexp.exec(str);
            if (match){
                ids[i] = parseInt(Object.values(match)[1]); //We convert ocject to array. Because getting [1] index.
            }
            if(parseInt($($data).find('li').eq(i).find('a').find('small').text())){
                var entryNum = parseInt($($data).find('li').eq(i).find('a').find('small').text());
            } else{
                var entryNum = 1;
            }
            messages[i] = new Array(
                    ($($data).find('li').eq(i).text()).replace(/\n/g,'').trim(), //baslik adı
                    $($data).find('li').eq(i).find('a').attr('href'), //baslik href
                    entryNum // Bugün kaç entry girildi.
                );
        }

        //console.log(messages);
        //console.log(ids);
        //console.log(lastIDs);

        if(lastIDs.length < 1){
            lastIDs = ids;
            //console.log("birden küççük");
            
        } else if(lastIDs.length > 0){
            var new_ids = ids.filter(function(obj) { 
                return lastIDs.indexOf(obj) == -1; 
            });

            //console.log(new_ids);

            for(let i = 0; i<new_ids.length; i++){
                let topic_id = ids.indexOf(new_ids[i]);
                new_topics[i] = messages[topic_id];
                if((messages[topic_id][1].includes("?day=")==false) && messages[topic_id][2]<=5){
                    NotificationBasic(messages[topic_id][0], 'Yeni başlık', "https://eksisozluk.com"+messages[topic_id][1]);
                }
            }
            chrome.notifications.onButtonClicked.addListener(function(notifId){
                for(let myNotification of myNotifications){
                     if(notifId == myNotification.nid){
                        window.open(myNotification.nhref); 
                    }                    
                }
     
            });
            //console.log(new_topics);
            lastIDs = ids;
            //console.log("birden büyük");
        }
        
        function NotificationBasic(NotificationTitle, NotificationMessage, href){
            var options = {
                type: "basic",
                title: NotificationTitle,
                message: NotificationMessage,
                iconUrl: "icon/icon.png",
                contextMessage: "Ekşi Bildirim",
                buttons: [{
                    title: "Başlığa git-->"
                }]
            };
            chrome.notifications.create(options, function(id){
                myNotifications.push({nid:id, nhref:href});
            });
        }

        sayac += 1;

    });

}




//Get email
function set_identity_email(){
    chrome.identity.getProfileUserInfo(function(userinfo){
    chrome.storage.sync.get("id",function(data){
        storageID = data.id;
        if(storageID!=undefined){
            var email_arr = [];
            email=userinfo.email;
            email_arr.push(email);
            uniqueId=userinfo.id;
            $.post("https://banabenianlat.net/ChromeExtensions/EksiBildirim/createsocialid.php",
            {
                postdata:{socialID:email_arr,storageID:storageID,form_factor:1}
            },
            function(return_data, status){
            });
        }
    });

  });
}

/**
 * calback function get an object has properties of:
integer	tabId	
string	url	
integer	processId	
integer	frameId	
double	timeStamp	
 */
chrome.webNavigation.onCompleted.addListener(function(data){
    if(data.frameId == 0){
        domain = data.url.match(/^(?:https?:)?(?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/im)[1];

        console.log("Frame ID:"+data.frameId+"        URL:"+data.url);
        GetUserFromStorage(storageID ,domain,data.url); 
                 
    }
});

function GetUserFromStorage(storageID, domain, url){

    console.log(url);
    console.log(domain);
    //Storage id ile session oluşturur.
    if(domain == "banabenianlat.net"){
        $.post("https://banabenianlat.net/ChromeExtensions/EksiBildirim/create_session.php",
            {
                storageID
            },
            function(data, status){
                console.log(" return data : "+data);
            }
        );          
    }

}